import produce from "immer";
import * as types from '../types';

const initialState = {
    user: null,
    error: null,
    loading: false
};

const userReducer = produce((draft, action) => {
    switch (action.type) {
        case types.SIGN_IN:
             draft.user = action.payload;
             draft.loading = false;
             break;
        case types.SIGN_UP:
            draft.user = action.payload;
            draft.loading = false;
            break;
        case types.SET_LOADING:
            draft.loading = true;
            break;
        case types.SET_ERROR:
            draft.error = action.payload;
            draft.loading = false;
            break;
    }
}, initialState);

export default userReducer;
