import rootReducers from "./reducers/rootReducers";
import {composeWithDevTools} from "redux-devtools-extension";
import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";

const middlewares = [thunk];

const store = createStore(rootReducers, composeWithDevTools(applyMiddleware(...middlewares)));

export default store;
