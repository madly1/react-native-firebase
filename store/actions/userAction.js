import * as types from '../types';
import firebase from "../../firebase/firebase.app";

export const signInUser = ({ email, password }) => async dispatch => {
    dispatch(setLoading());

    try {
        const res = await firebase.auth().signInWithEmailAndPassword(email, password);
        const user = await res.user;

        dispatch(signInUserDispatch(user))
    } catch (error) {
        const handleError = () => {
            switch (error.code) {
                case "auth/invalid-email":
                    return "Invalid email address format.";
                case "auth/user-not-found":
                case "auth/wrong-password":
                    return "Invalid email address or password.";
                case "auth/too-many-requests":
                    return "Too many request. Try again in a minute.";
                default:
                    return "Check your internet connection.";
            }
        };

        dispatch(setError(handleError()));
    }
};

export const signUpUser = ({ email, password }) => async dispatch => {
    dispatch(setLoading());

    try {
        const res = await firebase.auth().createUserWithEmailAndPassword(email, password);
        const user = await res.user;

        return dispatch(signUpUserDispatch(user));
    } catch (error) {

        const handleError = () => {
            switch (error.code) {
                case "auth/email-already-in-use":
                    return "E-mail already in use.";
                case "auth/invalid-email":
                    return "Invalid e-mail address format.";
                case "auth/weak-password":
                    return "Password is too weak.";
                case "auth/too-many-requests":
                    return "Too many request. Try again in a minute.";
                default:
                    return "Check your internet connection.";
            }
        };

        dispatch(setError(handleError()));
    }
};


export const signOutUser = () => dispatch => {
    dispatch(setLoading());
    firebase.auth().signOut();
};

export const signInUserDispatch = (data) => ({
    type: types.SIGN_IN,
    payload: data
});

export const signUpUserDispatch = (data) => ({
    type: types.SIGN_UP,
    payload: data
});

export const setLoading = () => ({
    type: types.SET_LOADING
});

export const setError = (error) => ({
    type: types.SET_ERROR,
    payload: error
});
