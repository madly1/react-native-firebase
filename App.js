import {StatusBar} from 'expo-status-bar';
import React from 'react';
import {AppRegistry} from 'react-native';
import {expo} from './app';
import {Provider as StoreProvider} from "react-redux";
import store from "./store";
import Main from "./src/main/Main";

export default function App() {
    return (
        <StoreProvider store={store}>
            <Main />
            <StatusBar style="auto"/>
        </StoreProvider>
    );
}

AppRegistry.registerComponent(expo.name, () => App);
