import firebaseApp from "./firebase.app";

const firebaseDB = firebaseApp.database();

export default firebaseDB;
