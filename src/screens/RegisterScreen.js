import React, {useState} from 'react';
import {View} from "react-native";
import {Button, Card, HelperText, TextInput} from "react-native-paper";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import {useDispatch} from "react-redux";
import {signUpUser} from "../../store/actions/userAction";

const RegisterScreen = ({ navigation }) => {
    const dispatch = useDispatch();

    const [form, setForm] = useState({
        email: '',
        password: ''
    });

    const [showPassword, setShowPassword] = useState(true);

    const onChangeForm = (key, val) => {
        setForm(prevState => ({
            ...prevState,
            [key]: val
        }));
    };

    const hasErrors = () => {
        return form.email && !form.email.includes('@');
    };

    const onRegister = () => {
        dispatch(signUpUser(form));
    };

    return (
        <View style={{flex: 1, justifyContent: 'center', paddingHorizontal: 20}}>
            <Card>
                <Card.Content>
                    <View>
                        <View style={{ alignItems: 'center', flexDirection: 'row' }}>
                            <MaterialCommunityIcons style={{ marginTop: 8, paddingRight: 14 }} name="email" size={24} />
                            <TextInput
                                autoFocus
                                label="Email"
                                placeholder="Email"
                                mode="outlined"
                                style={{ flex: 1 }}
                                value={form.email}
                                onChangeText={text => onChangeForm('email', text)}
                            />
                        </View>
                        <HelperText type="error" visible={hasErrors()} style={{ paddingHorizontal: 52 }}>
                            Email address is invalid!
                        </HelperText>
                    </View>
                    <View>
                        <View style={{ alignItems: 'center', flexDirection: 'row' }}>
                            <MaterialCommunityIcons style={{ marginTop: 8, paddingRight: 14 }}  name="lock" size={24} />
                            <TextInput
                                label="Password"
                                mode="outlined"
                                type="password"
                                style={{ flex: 1 }}
                                placeholder="Password"
                                value={form.password}
                                secureTextEntry={showPassword}
                                right={<TextInput.Icon name={showPassword ? 'eye' : 'eye-off'} onPress={() => setShowPassword(!showPassword)} forceTextInputFocus={false} />}
                                onChangeText={text => onChangeForm('password', text)}
                            />
                        </View>
                        <HelperText type="error" visible={hasErrors()} style={{ paddingHorizontal: 52 }}>
                            Password is invalid!
                        </HelperText>
                    </View>
                </Card.Content>
                <Card.Actions style={{ flexDirection: 'column', justifyContent: 'center' }}>
                    <Button
                        color="black"
                        mode="outlined"
                        onPress={onRegister}
                    >
                        Register
                    </Button>
                    <Button
                        color="blue"
                        style={{ marginTop: 8 }}
                        onPress={() => navigation.navigate('Login')}
                    >
                        Go to Login
                    </Button>
                </Card.Actions>
            </Card>
        </View>
    );
};

export default React.memo(RegisterScreen);
