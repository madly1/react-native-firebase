import React from 'react';
import {View} from 'react-native';
import {Button} from "react-native-paper";
import {useDispatch} from "react-redux";
import {signOutUser} from "../../store/actions/userAction";

const DashboardScreen = () => {
    const dispatch = useDispatch();

    const onLogOut = () => {
        dispatch(signOutUser());
    };

    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Button mode="outlined" onPress={onLogOut}>
                Logout
            </Button>
        </View>
    );
};

export default React.memo(DashboardScreen);
