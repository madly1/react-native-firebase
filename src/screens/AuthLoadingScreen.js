import React from "react";
import firebase from "../../firebase/firebase.app";
import Loading from "../components/Loading";

const AuthLoadingScreen = ({ navigation }) => {
    firebase.auth().onAuthStateChanged(user => {
        if (user) {
            // User is logged in
            navigation.navigate("Dashboard");
        } else {
            // User is not logged in
            navigation.navigate("Login");
        }
    });

    return <Loading />
};

export default React.memo(AuthLoadingScreen);
