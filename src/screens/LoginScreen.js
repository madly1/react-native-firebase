import React, {useState} from 'react';
import {View} from "react-native";
import {TextInput, Button, Card, HelperText} from "react-native-paper";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import {signInUser} from "../../store/actions/userAction";
import {useDispatch, useSelector} from "react-redux";

const LoginScreen = ({navigation}) => {
    const dispatch = useDispatch();
    const error = useSelector(({ user }) => user.error);

    const [form, setForm] = useState({
        email: '',
        password: ''
    });

    const [showPassword, setShowPassword] = useState(true);

    const onChangeForm = (key, val) => {
        setForm(prevState => ({
            ...prevState,
            [key]: val
        }));
    };

    const hasErrors = () => {
        return error;
    };

    const onLogin = () => {
        dispatch(signInUser(form));
    };

    return (
        <View style={{flex: 1, justifyContent: 'center', paddingHorizontal: 20}}>
            <Card>
                {/*<Card.Title title={error} titleStyle={{ color: 'red' }} />*/}
                <Card.Content>
                    <View>
                        <View style={{alignItems: 'center', flexDirection: 'row'}}>
                            <MaterialCommunityIcons style={{marginTop: 8, paddingRight: 14}} name="email" size={24}/>
                            <TextInput
                                autoFocus
                                label="Email"
                                error={!form.email || !form.email.includes('@')}
                                placeholder="Email"
                                mode="outlined"
                                style={{flex: 1}}
                                value={form.email}
                                onChangeText={text => onChangeForm('email', text)}
                            />
                        </View>
                    </View>
                    <View>
                        <View style={{alignItems: 'center', flexDirection: 'row'}}>
                            <MaterialCommunityIcons style={{marginTop: 8, paddingRight: 14}} name="lock" size={24}/>
                            <TextInput
                                label="Password"
                                mode="outlined"
                                type="password"
                                error={!form.password}
                                style={{flex: 1}}
                                placeholder="Password"
                                value={form.password}
                                secureTextEntry={showPassword}
                                right={<TextInput.Icon name={showPassword ? 'eye' : 'eye-off'}
                                                       onPress={() => setShowPassword(!showPassword)}
                                                       forceTextInputFocus={false}/>}
                                onChangeText={text => onChangeForm('password', text)}
                            />
                        </View>
                    </View>
                    <HelperText style={{ textAlign: 'center' }} type="error" visible={hasErrors()}>
                        {error}
                    </HelperText>
                </Card.Content>
                <Card.Actions style={{flexDirection: 'column', justifyContent: 'center'}}>
                    <Button
                        color="black"
                        mode="outlined"
                        onPress={onLogin}
                    >
                        Login
                    </Button>
                    <Button
                        color="blue"
                        style={{marginTop: 8}}
                        onPress={() => navigation.navigate('Register')}
                    >
                        Go to Register
                    </Button>
                </Card.Actions>
            </Card>
        </View>
    );
};

export default React.memo(LoginScreen);
